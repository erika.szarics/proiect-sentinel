from azure.identity import DefaultAzureCredential
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.resource.resources.models import Deployment
from azure.mgmt.resource.resources.models import DeploymentMode

# Set your Azure subscription ID and resource group name
subscription_id = "3febda79-3642-4c2d-92cd-1876489fe994"
resource_group_name = "dobarb-sentinel"

# Set your Azure credentials
credential = DefaultAzureCredential()

# Set the Azure Sentinel deployment details
deployment_name = "CreateVm-jetware-srl.wordpress4_lemp7-wordpress4_-20230617135726"
location = "East US"
workspace_id = "0b9334d5-0f44-41d7-a035-0b610d4b83d5"

# Set the ARM template and parameters for Azure Sentinel deployment
template_uri = "https://raw.githubusercontent.com/Azure/Azure-Sentinel/master/Sentinel%20Deploy/azuredeploy.json"
parameters = {
    "workspaceId": {
        "value": workspace_id
    }
}

# Create the Resource Management client
resource_client = ResourceManagementClient(credential, subscription_id)

# Create the deployment properties
deployment_properties = Deployment(properties=DeploymentMode.incremental)

# Start the deployment
deployment_async_operation = resource_client.deployments.begin_create_or_update(
    resource_group_name,
    deployment_name,
    deployment_properties,
    {
        "template": template_uri,
        "parameters": parameters
    }
)
deployment_result = deployment_async_operation.result()

print("Azure Sentinel deployment completed.")
