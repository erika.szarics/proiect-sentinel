from azure.identity import DefaultAzureCredential
from azure.mgmt.logic import LogicManagementClient
from azure.mgmt.logic.models import Workflow, WorkflowProperties, WorkflowTriggerSchema

# Set your Azure subscription ID and resource group name
subscription_id = "3febda79-3642-4c2d-92cd-1876489fe994"
resource_group_name = "dobarb-sentinel"

# Set your Azure credentials
credential = DefaultAzureCredential()

# Set the Logic App details
logic_app_name = "dobarb-wordpress-demo"
location = "East US"

# Set the workflow definition for the Logic App
workflow_definition = {
    "$schema": "https://schema.management.azure.com/providers/Microsoft.Logic/schemas/2016-06-01/workflowdefinition.json#",
    "actions": {},
    "triggers": {
        "manual": {
            "type": "Request",
            "kind": "Http",
            "inputs": {
                "schema": {}
            }
        }
    },
    "contentVersion": "1.0.0.0",
    "outputs": {}
}

# Create the Logic Management client
logic_client = LogicManagementClient(credential, subscription_id)

# Create the workflow properties
workflow_properties = WorkflowProperties(
    definition=workflow_definition,
    state="Enabled",
    location=location
)

# Create the workflow
workflow = Workflow(
    location=location,
    properties=workflow_properties
)

# Create or update the Logic App
logic_client.workflows.create_or_update(
    resource_group_name,
    logic_app_name,
    workflow
)

print("Logic App deployment completed.")
