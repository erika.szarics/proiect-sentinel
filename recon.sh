#!/bin/bash

#######################################################################################
# This script is designed to perform the reconnaissance step in a cybersecurity attack,
# using the tool Nmap. Based on the OS type the script is currently running on, it will
# check if the mentioned service is already installed or not, and if not, it will 
# install it. The script then proceeds to scan the chosen target and output the results.
#######################################################################################


#######################################################################################
# Global variables to be changed based on the IP address and port of the target machine
###################################################################################### 
IP="192.168.0.103"
PORT="8080"


######################################################################################
# Function that determines the OS and its version
######################################################################################
check_os_version() {
    
    os_name=$(grep -oP '(?<=^NAME=")(.*)(?=")' /etc/os-release)
    os_version=$(grep -oP '(?<=^VERSION_ID=")(.*)(?=")' /etc/os-release)

    
    echo "OS Name: $os_name"
    echo "OS Version: $os_version"

    
    if [[ "$os_name" == "Ubuntu" ]]; then
        echo "This is Ubuntu."
        type=1
    elif [[ "$os_name" == "Debian" ]]; then
        echo "This is Debian."
        type=1
    elif [[ "$os_name" == "Kali GNU/Linux" ]]; then
        echo "This is Kali Linux."
        type=1
    elif [[ "$os_name" == "Linux Mint" ]]; then
        echo "This is Linux Mint."
        type=1
    elif [[ "$os_name" == "Fedora" ]]; then
        echo "This is Fedora."
        type=2
    elif [[ "$os_name" == "CentOS Linux" ]]; then
        echo "This is CentOS."
        type=2
    elif [[ "$os_name" == "Red Hat Enterprise Linux" ]]; then
        echo "This is Red Hat Enterprise Linux (RHEL)."
        type=2
    elif [[ "$os_name" == "Arch Linux" ]]; then
        echo "This is Arch Linux."
        type=3
    elif [[ "$os_name" == "Manjaro Linux" ]]; then
        echo "This is Manjaro Linux."
        type=3
    elif [[ "$os_name" == "openSUSE" ]]; then
        echo "This is openSUSE."
        type=4
    else
        echo "Please check the online documentation to complete the installation."
        exit 0
    fi

    check_service "$type"
}


######################################################################################
# Function that determines if the service is installed or not
######################################################################################
check_service() {
    local type=$1

    if [ "$type" == "1" ]; then
        dpkg -s nmap &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Nmap is installed."
            run
        else
            echo "Installing Nmap..."
            install_service "$type"
        fi
    elif [ "$type" == "2" ]; then
        rpm -q nmap &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Nmap is installed."
            run
        else
            echo "Installing Nmap..."
            install_service "$type"
        fi
    elif [ "$type" == "3" ]; then
        pacman -Qi nmap &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Nmap is installed."
            run
        else
            echo "Installing Nmap..."
            install_service "$type"
        fi
    elif [ "$type" == "4" ]; then
        zypper search -i nmap &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Nmap is installed."
            run
        else
            echo "Installing Nmap..."
            install_service "$type"
        fi
    fi
}


######################################################################################
# Function that installs the service
######################################################################################
install_service() {
    local type=$1

    if [ "$type" == "1" ]; then
        sudo apt update
        sudo apt install nmap
        run
    elif [ "$type" == "2" ]; then
        sudo dnf install nmap
        run
    elif [ "$type" == "3" ]; then
        sudo pacman -Sy nmap
        run
    elif [ "$type" == "4" ]; then
        sudo zypper install nmap
        run
    fi
}


######################################################################################
# Function that tries scanning the target machine for possible vulnerabilities
######################################################################################
run() {
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo "NMAP SCANNING THE TARGET AGGRESSIVELY" | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    sudo nmap -T4 "$IP" | tee -a security_report.txt | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo "NMAP SCANNING THE TARGET FOR POSSIBLE WORDPRESS SITE" | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    sudo nmap -p "$PORT" -A "$IP" | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo "The results of the scan can be found in the security_report.txt file."
}


######################################################################################
# The main function
######################################################################################
main() {
    check_os_version
}


main
