import requests
from azure.identity import DefaultAzureCredential

# Set your Azure subscription ID and resource ID
subscription_id = "3febda79-3642-4c2d-92cd-1876489fe994"
resource_id = "/subscriptions/3febda79-3642-4c2d-92cd-1876489fe994/resourceGroups/dobarb-sentinel/providers/Microsoft.OperationalInsights/sentinel-demo"

# Set your Azure credentials
credential = DefaultAzureCredential()

# Set the Azure Security Center API endpoint
api_version = "2021-01-01"
endpoint = f"https://management.azure.com{resource_id}/providers/Microsoft.Security/{api_version}/subAssessments"

# Set the request headers
headers = {
    "Authorization": f"Bearer {credential.get_token('https://management.azure.com/.default').token}",
    "Content-Type": "application/json"
}

# Set the request payload
payload = {
    "properties": {
        "status": "On"
    }
}

# Send the API request to enable Azure Defender for the resource
response = requests.put(endpoint, headers=headers, json=payload)

if response.status_code == 200:
    print("Azure Defender for Cloud enabled successfully.")
else:
    print(f"Failed to enable Azure Defender for Cloud. Status code: {response.status_code}, Error: {response.text}")
