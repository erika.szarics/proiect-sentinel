from azure.identity import DefaultAzureCredential
from azure.mgmt.loganalytics import LogAnalyticsManagementClient
from azure.mgmt.loganalytics.models import Workspace

# Set your Azure subscription ID and resource group name
subscription_id = "3febda79-3642-4c2d-92cd-1876489fe994"
resource_group_name = "dobarb-sentinel"

# Set your Azure credentials
credential = DefaultAzureCredential()

# Set the Log Analytics workspace details
workspace_name = "sentinel-demo"
location = "East US"
sku = "PerGB2018"  # Workspace SKU, e.g., "PerGB2018" for Pay-as-you-go or "Standalone" for capacity reservation

# Create the Log Analytics management client
log_analytics_client = LogAnalyticsManagementClient(credential, subscription_id)

# Create the Log Analytics workspace object
workspace = Workspace(location=location, sku=sku)

# Create or update the Log Analytics workspace
log_analytics_client.workspaces.create_or_update(resource_group_name, workspace_name, workspace)

print("Log Analytics workspace deployment completed.")
